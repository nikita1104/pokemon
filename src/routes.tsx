import React from "react";
import { Switch, Route } from "react-router-dom";
import { PokemonsList } from "./components/PokemonsList";
import { Pokemon } from "./components/Pokemon";
import { Ability } from "./components/Ability";
import { NotFound } from "./components/NotFound";

export const Routes = () => (
  <Switch>
    <Route exact path="/">
      <PokemonsList />
    </Route>
    <Route path="/pokemon/:id">
      <Pokemon />
    </Route>
    <Route path="/ability/:id">
      <Ability />
    </Route>
    <Route component={NotFound} />
  </Switch>
);
