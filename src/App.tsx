import React from "react";
import { Routes } from "./routes";
import "./App.scss";
import Logo from "./images/logo.png";

function App() {
  return (
    <div className="App">
      <header className="header">
        <div className="container">
          <img src={Logo} alt="logo" />
        </div>
      </header>
      <div className="main-content">
        <div className="container">
          <div className="main-content__inner">
            <Routes />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
