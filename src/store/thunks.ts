import axios from "axios";
import { actions } from "./actions";

export const thunkGetPokemonsList = (dispatch: any): any =>
  // ThunkAction<void, Array<string>, unknown, Action<string>>
  // async (dispatch: any) =>
  {
    const asyncResp = api.getPokemons();
    asyncResp
      .then((response) => {
        dispatch(
          actions.pokemonsList.success(
            response.data.results.map(
              (pokemon: { name: string }) => pokemon.name
            )
          )
        );
      })
      .catch(() => {
        dispatch(actions.pokemonsList.error());
      });
  };

export const thunkGetPokemonsBase = (dispatch: any, name: string): any =>
  // ThunkAction<void, Array<string>, unknown, Action<string>>
  // async (dispatch: any) =>
  {
    const asyncResp = api.getPOkemonBase(name);
    asyncResp
      .then((response) => {
        dispatch(actions.pokemonDetails.success(response.data));
      })
      .catch(() => {
        dispatch(actions.pokemonsList.error());
      });
  };

const api = {
  getPokemons: () =>
    axios(`https://pokeapi.co/api/v2/pokemon`, {
      params: {
        limit: 1000,
      },
    }),
  getPOkemonBase: (name: string) =>
    axios(`https://pokeapi.co/api/v2/pokemon/${name}`),
};
