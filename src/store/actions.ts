import {
  GET_POKEMONS_INIT,
  GET_POKEMONS_SUCCESS,
  GET_POKEMONS_ERROR,
  GET_POKEMONS_FILTER,
  GET_POKEMONS_SET_PAGE,
  GET_POKEMON_INIT,
  GET_POKEMON_SUCCESS,
  // GET_POKEMON_ERROR,
  // GET_ABILITY_INIT,
  // GET_ABILITY_SUCCESS,
  // GET_ABILITY_ERROR,
} from "./types";
import { createWorkData } from "../helpers/createWorkData";

const pokemonsList = {
  init: () => ({
    type: GET_POKEMONS_INIT,
  }),
  success: (pokemons: Array<string>) => ({
    type: GET_POKEMONS_SUCCESS,
    payload: pokemons,
    pages: createWorkData(pokemons),
  }),
  error: () => ({
    type: GET_POKEMONS_ERROR,
  }),
  filter: (name: string) => ({
    type: GET_POKEMONS_FILTER,
    payload: name,
  }),
  setPage: (page: number) => ({
    type: GET_POKEMONS_SET_PAGE,
    payload: page,
  }),
};

const pokemonDetails = {
  init: () => ({
    type: GET_POKEMON_INIT,
  }),
  success: (pokemons: any) => ({
    type: GET_POKEMON_SUCCESS,
    payload: pokemons,
    // pages: createWorkData(pokemons),
  }),
};

export const actions = {
  pokemonsList,
  pokemonDetails,
};
