import { combineReducers } from "redux";
import {
  GET_POKEMONS_INIT,
  GET_POKEMONS_SUCCESS,
  GET_POKEMONS_ERROR,
  GET_POKEMONS_FILTER,
  GET_POKEMONS_SET_PAGE,
  GET_POKEMON_INIT,
  GET_POKEMON_SUCCESS,
  GET_POKEMON_ERROR,
  GET_ABILITY_INIT,
  GET_ABILITY_SUCCESS,
  GET_ABILITY_ERROR,
} from "./types";
import { createWorkData } from "../helpers/createWorkData";

interface pokemonsList {
  loading: boolean;
  dataResponse: null | Array<string>;
  error: boolean;
  filter: string;
  page: number;
  pages: null | {
    data: Array<string>;
    pages: number;
  };
}

export const PokemonsListReducer = (
  state: pokemonsList = {
    loading: false,
    dataResponse: null,
    filter: "",
    error: false,
    page: 0,
    pages: null,
  },
  action: any
) => {
  switch (action.type) {
    case GET_POKEMONS_INIT:
      return {
        ...state,
        loading: true,
        dataResponse: null,
        workData: null,
        error: false,
      };
    case GET_POKEMONS_SUCCESS:
      return {
        ...state,
        loading: false,
        dataResponse: action.payload,
        pages: action.pages,
        pokemons: action.pages.data[state.page],
        error: null,
      };
    case GET_POKEMONS_ERROR:
      return {
        ...state,
        loading: false,
        dataResponse: null,
        workData: null,
        error: true,
      };
    case GET_POKEMONS_FILTER:
      return {
        ...state,
        filterSubstr: action.payload,
        pages: createWorkData(
          state.dataResponse?.filter((name) =>
            new RegExp(action.payload).test(name)
          )
        ),
        page: 0,
        pokemons: (() => {
          const filtered = state.dataResponse?.filter((name) =>
            new RegExp(action.payload).test(name)
          );
          if (filtered?.length !== 0) {
            return createWorkData(
              state.dataResponse?.filter((name) =>
                new RegExp(action.payload).test(name)
              )
            ).data[0];
          } else {
            return [];
          }
        })(),
      };
    case GET_POKEMONS_SET_PAGE:
      return {
        ...state,
        page: action.payload,
        pokemons: (() => {
          return state.pages?.data[action.payload];
        })(),
      };
    default:
      return state;
  }
};

export const PokemonReducer = (
  state = {
    loading: false,
    data: null,
    error: null,
  },
  action: any
) => {
  switch (action.type) {
    case GET_POKEMON_INIT:
      return {
        loading: true,
        data: null,
        error: null,
      };
    case GET_POKEMON_SUCCESS:
      return {
        loading: false,
        data: action.payload,
        error: null,
      };
    case GET_POKEMON_ERROR:
      return {
        loading: false,
        data: null,
        error: "error",
      };
    default:
      return state;
  }
};

export const AbilityReducer = (
  state = {
    loading: false,
    data: null,
    error: null,
  },
  action: any
) => {
  switch (action.type) {
    case GET_ABILITY_INIT:
      return {
        loading: true,
        data: null,
        error: null,
      };
    case GET_ABILITY_SUCCESS:
      return {
        loading: false,
        data: action.payload,
        error: null,
      };
    case GET_ABILITY_ERROR:
      return {
        loading: false,
        data: null,
        error: "error",
      };
    default:
      return state;
  }
};

export const rootReducer = combineReducers({
  pokemonsList: PokemonsListReducer,
  pokemon: PokemonReducer,
  ability: AbilityReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
