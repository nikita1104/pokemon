// interface workDataType {
//   page: number;
//   data: string;
// }

export const createWorkData = (
  array: string[] | undefined
  // page: number,
  // filter: string
): any => {
  if (!array || array.length === 0)
    return {
      data: [],
      pages: 0,
    };

  let size = 20;
  let subarray = [];
  for (let i = 0; i < Math.ceil(array.length / size); i++) {
    subarray[i] = array.slice(i * size, i * size + size);
  }

  return { data: subarray, pages: subarray.length };
};
