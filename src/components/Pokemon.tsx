import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";
import {
  GET_POKEMON_INIT,
  // GET_POKEMON_SUCCESS,
  // GET_POKEMON_ERROR,
} from "../store/types";
import { Link } from "react-router-dom";
import { thunkGetPokemonsBase } from "../store/thunks";
// import axios from "axios";
import { GoBack } from "./atoms/GoBack";

const Component = ({ getPokemon, pokemon }: any): any => {
  let { id } = useParams();
  const { loading, data, error } = pokemon;
  // const {};
  // console.log("data", data);

  useEffect(() => {
    getPokemon(id);
  }, [getPokemon, id]);
  return (() => {
    if (loading) return "loading";
    if (data)
      return (
        <div className="pokemon-details">
          <GoBack />
          <div className="pokemon-details__main">
            <div className="pokemon-details__column">
              <h1>{data.name}</h1>
              <div>
                <div
                  style={{
                    backgroundImage: `url(${data.sprites.front_default})`,
                  }}
                  className="pokemon-image pokemon-image_indetails"
                />
              </div>
              <div className="">
                <div className="abilities">
                  {data.abilities.map(({ ability }: any, i: any) => (
                    <div key={i}>
                      <Link
                        to={`/ability/${ability.name}`}
                        className="abilities__item"
                      >
                        {ability.name}
                      </Link>
                    </div>
                  ))}
                </div>
              </div>
            </div>
            <div className="pokemon-details__column">
              <div>
                <div className="pokemon-details__params characteristics">
                  <div className="characteristics__column">
                    <ul>
                      {data.stats.map(
                        (
                          item: {
                            base_stat: number;
                            effort: number;
                            stat: { name: string };
                          },
                          i: number
                        ) => (
                          <li key={i} className="characteristics__item">
                            <div>
                              <div className="characteristics__item-title">
                                {item.stat.name}
                              </div>
                              <div className="characteristics__item-description">
                                {item.base_stat}
                              </div>
                            </div>
                          </li>
                        )
                      )}
                    </ul>
                  </div>
                  <div className="characteristics__column">
                    <ul>
                      {[
                        {
                          title: "height",
                          description: data.height,
                        },
                        {
                          title: "Weight",
                          description: data.weight,
                        },
                        {
                          title: "Type",
                          description: data.types
                            .map(
                              (item: { type: { name: string } }, i: number) =>
                                item.type.name
                            )
                            .join(", "),
                        },
                      ].map((item, i) => (
                        <li className="characteristics__item" key={i}>
                          <div className="characteristics__item-title">
                            {item.title}
                          </div>
                          <div className="characteristics__item-description">
                            {item.description}
                          </div>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <pre>{JSON.stringify(data, null, " ")}</pre> */}
        </div>
      );
    if (error) return "error";
    return null;
  })();
};

const mapStateToProps = ({ pokemon }: any) => ({ pokemon });
const mapDispatchToProps = (dispatch: any) => ({
  getPokemon: (name: string) => {
    dispatch({
      type: GET_POKEMON_INIT,
    });
    thunkGetPokemonsBase(dispatch, name);
  },
});

export const Pokemon = connect(mapStateToProps, mapDispatchToProps)(Component);
