import React, { useEffect, useRef } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Filter } from "./Filter";
import { actions } from "../store/actions";
import { thunkGetPokemonsList } from "../store/thunks";
import ReactPaginate from "react-paginate";

const scrollToRef = (ref: any) =>
  window.scrollTo({
    top: ref.current.offsetTop,
    behavior: "smooth",
  });

const Component = (props: any): any => {
  const { getPokemons, filter, pokemonsList, setPage } = props;
  const { loading, error, pokemons, pages, page } = pokemonsList;
  const Ref = useRef(null);
  const executeScroll = () => scrollToRef(Ref);

  useEffect(() => {
    getPokemons();
  }, [getPokemons]);
  return (() => {
    if (loading) return "loading";
    if (pokemons)
      return (
        <div ref={Ref}>
          <Filter
            onFilterSubmit={({ pokemonName }: { pokemonName: string }) => {
              // console.log(pokemonName);
            }}
            onChangeNameField={(pokemonName: string) => {
              filter(pokemonName);
            }}
          />
          {pokemons.length ? <ViewList data={pokemons} /> : "Pokemon not found"}
          {pages.data.length > 1 && (
            <ReactPaginate
              previousLabel={null}
              nextLabel={null}
              breakLabel={"..."}
              breakClassName={"break-me"}
              pageCount={pages.data.length}
              marginPagesDisplayed={2}
              pageRangeDisplayed={3}
              onPageChange={({ selected }) => {
                setPage(selected);
                executeScroll();
              }}
              containerClassName={"pagination"}
              forcePage={page}
              activeClassName={"active"}
              disabledClassName="arrows"
            />
          )}
        </div>
      );
    if (error) return "error";
    return null;
  })();
};

const ViewList = ({ data }: { data: Array<any> }) => (
  <div className="pokemons-list">
    {data.map((pokemonName: string, i: number) => (
      <div key={i} className="pokemons-list__pokemon">
        <Link to={`/pokemon/${pokemonName}`}>
          <div
            style={{
              backgroundImage: `url(https://via.placeholder.com/350/616161/fff?text=${pokemonName})`,
            }}
            className="pokemon-image pokemon-image_inlist"
          />
          <div className="pokemons-list__name">{pokemonName}</div>
        </Link>
      </div>
    ))}
  </div>
);

const mapStateToProps = ({ pokemonsList }: any) => ({
  pokemonsList,
});
const mapDispatchToProps = (dispatch: any) => ({
  getPokemons: () => {
    dispatch(actions.pokemonsList.init());
    thunkGetPokemonsList(dispatch);
  },
  filter: (name: string) => {
    dispatch(actions.pokemonsList.filter(name));
  },
  setPage: (page: number) => {
    dispatch(actions.pokemonsList.setPage(page));
  },
});

export const PokemonsList = connect(
  mapStateToProps,
  mapDispatchToProps
)(Component);
