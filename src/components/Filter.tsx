import React from "react";
import { Form, Field } from "react-final-form";

export const Filter = ({
  onFilterSubmit,
  onChangeNameField,
}: {
  onFilterSubmit: any;
  onChangeNameField: any;
}) => (
  <div className="pokemons-filter">
    <Form
      onSubmit={onFilterSubmit}
      render={({ handleSubmit, values }) => (
        <form onSubmit={handleSubmit}>
          <div>
            <Field name="pokemonName">
              {(props) => (
                <div>
                  <input
                    name={props.input.name}
                    value={props.input.value}
                    placeholder="Pokemon Name"
                    className="input"
                    autoComplete="off"
                    onChange={(e) => {
                      props.input.onChange(e);
                      onChangeNameField(e.target.value);
                    }}
                  />
                </div>
              )}
            </Field>
          </div>
          {/* <button type="submit">Find</button> */}
        </form>
      )}
    />
  </div>
);
