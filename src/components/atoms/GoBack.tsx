import React from "react";
import { useHistory } from "react-router-dom";

export const GoBack = () => {
  const history = useHistory();
  return (
    <button onClick={() => history.goBack()} className="bo-back-button">
      {"<"}return
    </button>
  );
};
