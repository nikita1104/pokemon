import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";
import {
  GET_ABILITY_INIT,
  GET_ABILITY_SUCCESS,
  GET_ABILITY_ERROR,
} from "../store/types";
import axios from "axios";
import { GoBack } from "./atoms/GoBack";

export const Component = (props: any): any => {
  const { getAbility, ability } = props;
  let { id } = useParams();
  const { loading, data, error } = ability;
  useEffect(() => {
    getAbility(id);
  }, [getAbility, id]);
  return (() => {
    if (loading) return "loading";
    if (data)
      return (
        <div>
          <h1>{data.name}</h1>
          <p>
            {
              data.effect_entries.find(
                (item: { language: { name: string }; effect: string }) =>
                  item.language.name === "en"
              ).effect
            }
          </p>
          <p>
            {
              data.flavor_text_entries.filter(
                (item: { language: { name: string }; flavor_text: string }) =>
                  item.language.name === "en"
              )[0].flavor_text
            }
          </p>
          <GoBack />
        </div>
      );
    if (error) return "error";
    return null;
  })();
};

const mapStateToProps = ({ ability }: any) => ({ ability });
const mapDispatchToProps = (dispatch: any) => ({
  getAbility: (name: string) => {
    dispatch({
      type: GET_ABILITY_INIT,
    });
    axios(`https://pokeapi.co/api/v2/ability/${name}`)
      .then((response: any) => {
        dispatch({
          type: GET_ABILITY_SUCCESS,
          payload: response.data,
        });
      })
      .catch(() => {
        dispatch({
          type: GET_ABILITY_ERROR,
        });
      });
  },
});

export const Ability = connect(mapStateToProps, mapDispatchToProps)(Component);
